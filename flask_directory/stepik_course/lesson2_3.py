#ispolzowanije szablona Jinja2
#nastojka rezima otladki debugpri razrobotki projekta udobno sledic za oshibkami

from flask import Flask, render_template

app = Flask(__name__)


@app.route("/")
def index():
    return render_template("index")


if __name__ == "__main__":
    app.run(debug=True)
