# ex1 Convert two lists into a dictionary .Below are the two lists. Write a Python program to convert them into a
# dictionary in a way that item from list1 is the key and item from list2 is the value
def do_dictionary_from_two_lists(key, value):
    my_dict1 = {}
    for i in range(len(key)):
        my_dict1[key[i]] = value[i]
    print("ex1:", my_dict1)


keys = ['Ten', 'Twenty', 'Thirty']
values = [10, 20, 30]
do_dictionary_from_two_lists(keys, values)


# ex2 Merge two Python dictionaries into one.
def do_one_dictionary_from_two_dictionary(dict_1, dict_2):
    dict_1.update(dict_2)
    print("ex2:", dict_1)


dict1 = {'Ten': 10, 'Twenty': 20, 'Thirty': 30}
dict2 = {'Thirty': 30, 'Fourty': 40, 'Fifty': 50}
do_one_dictionary_from_two_dictionary(dict1, dict2)


# ex3 Print the value of key ‘history’ from the below dict
def print_value_of_key_history(sample_dict):
    x = sample_dict["class"]["student"]['marks']["history"]
    print("ex3:", x)


sampleDict = {"class": {"student": {"name": "Mike", "marks": {"physics": 70, "history": 80}}}}
print_value_of_key_history(sampleDict)


# ex4 In Python, we can initialize the keys with the same values.  Initialize dictionary with default values.
def do_one_dictionary_from_list_and_dictionary(list, dict):
    my_dictionary = {}
    my_dictionary = dict.fromkeys(list, dict)
    print("ex4:", my_dictionary)


employees = ['Kelly', 'Emma']
defaults = {"designation": 'Developer', "salary": 8000}
do_one_dictionary_from_list_and_dictionary(employees, defaults)


#  ex5 Write a Python program to create a new dictionary by extracting the mentioned keys from the below dictionary.
def print_key_values_from_dictionary(dict, list):
    new_dict = {}
    for i in range(len(list)):
        new_dict[list[i]] = dict[list[i]]
    print("ex5:", new_dict)


sample_dict = {"name": "Kelly", "age": 25, "salary": 8000, "city": "New york"}
keys = ["name", "salary", "age"]
print_key_values_from_dictionary(sample_dict, keys)


#  ex6  Delete a list of keys from a dictionary
def delete_a_list_of_key(dictionary, list):
    for i in range(len(list)):
        del dictionary[list[i]]
    print("ex6:", dictionary)


sample_dict = {"name": "Kelly", "age": 25, "salary": 8000, "city": "New york"}
keys = ["name", "salary"]
delete_a_list_of_key(sample_dict, keys)


#  ex7 Check if a value exists in a dictionary. Return True if it is and false if it is not.
def is_value_in_a_dict(dictionary):
    for i in dictionary:
        if dictionary[i] == 200:
            print("ex7:", end=" ")
            return True
    print("ex7:", end=" ")
    return False


samples_dict = {'a': 100, 'b': 200, 'c': 300, "d": 493}
print(is_value_in_a_dict(samples_dict))

#  ex8 Rename key of a dictionary. Write a program to rename a key "city" to a "location" in the following dictionary.
dict = {"name": "Kelly", "age": 25, "salary": 8000, "city": "New york"}
dict["location"] = dict["city"]  # or dict["location"] = dict.pop("city")
dict.pop("city")
print("ex8:", dict)


#  ex9   Get the key of a minimum value from the following dictionary.
def min_value_in_dictionary(dict):
    print("ex9:", min(dict, key=dict.get))


samplesi_dict = {'Physics': 82, 'Math': -3, 'history': 75, "English": -2, "Russian": 2}
min_value_in_dictionary(samplesi_dict)


# ex10 Change value of a key in a nested dictionary. Write a Python program to change Brad’s salary to 8500 in the
# following dictionary.
def change_value_of_a_key(dict):
    dict["emp3"]['salary'] = 8500
    print("ex10:", dict)


nested_dict = {'emp1': {'name': 'Jhon', 'salary': 7500}, 'emp2': {'name': 'Emma', 'salary': 8000},
               'emp3': {'name': 'Brad', 'salary': 500}}
change_value_of_a_key(nested_dict)