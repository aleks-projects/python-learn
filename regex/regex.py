import re

# \w - [a-zA-z0-9_]
# \d - [0-9]
# \D - [^0-9]
# \s - [\t\n\r\f\v]
# \S - [^\t\n\r\f\v]
# '+' = {1, inf} - symbol '+' means one and more symbols
# '*' = {0, inf} - symbol '*' means zero and more symbols
# '.' - any symbol except \n

text = "My name is alex"
match = re.findall(r"[A-Z][a-z]",text) # findall method find all regexes from string and return array with regexes
print("1:", match)


text_2 = "Pythooon, pythooon, panteon, greon, Palon"
match_2 = re.findall(r"[Pp]\w*on", text_2)
print("2:", match_2)


text_3 = "Gooooogle, goooooooooooogle, google, Gogle"
match_3 = re.findall(r"[Gg]o{2}gle",text_3)
match_4 = re.findall(r"[Gg]o{1,5}gle",text_3)
print("3:", match_3)
print("4:", match_4)

text_4 = 'lat=5, lot = 7, cat= ,day =14'
match_5 = re.findall(r"\w+\s*=\s*\d+",text_4)
match_6 = re.findall(r"(lat|lot)\s*=\s*(\d+)", text_4)
match_7 = re.findall(r"(?:lat|lot)\s*=\s*(\d+)", text_4)
print("5:", match_5)
print("6:", match_6)
print("7:", match_7)

text_5 = '"name": "Alex", "surename": "Brown", "age" = 34 '

match_8 = re.findall(r"[\"\']+(\w+)[\"\']+\s*[:=]\s*[\"\']+(\w+)[\"\']+",text_5)
print("8:", match_8)


#------------------------------

#method search(), fullmatch(), match() return Match object
# method search() find only first regex
# method match() search regex only at the beginning of the sting
# method fullmatch() - checks if the entire string corresponds to the regex

# object methods:
# group(n) - return group n
# groups() - return all groups
# groupdict() - return dictionary with named groups
# strart(n) - return first regex index, n is group number
# end(n) -  return last regex index, n is group number

text_6 = "20,     14,   51,      53,   12, 123, 1"
match_9 = re.search(r"\d{1,2},\s+\d{1,2}", text_6)
print(match_9)

text_7 = "Alex1234"
match_10 = re.fullmatch(r"A\w{3}\d{4}", text_7)
match_11 = re.fullmatch(r"a\w{3}\d{4}", text_7)
print("10:", match_10)
print("11:", match_11)


text_8 = "1alina"
match_12 = re.match(r"[0-9].+", text_8)
match_13 = re.match(r"[a-zA-Z].+", text_8)
print("12:", match_12)
print("13:", match_13)


# method sub() - is used to replace any symbols in string

text_9 = "name: Alex, surename: Brown, Age:20"
text_9 = re.sub(":\s*", "=", text_9)
print(text_9)


# method compile() - is used to compile your regex and use it in the future


regex = re.compile(r"(\d{3})")
text_10 = "123, 12, 51, 135, 13661, 415"
match_14 = regex.findall(text_10)
print("14:", match_14)





