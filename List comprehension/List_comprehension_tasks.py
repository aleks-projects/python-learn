# example to show how branch works from branch-test


# ex1 Find all of the numbers from 1-50 that are divisible by 7.
lista1 = [i for i in range(1, 50)]
lista_1 = [x for x in lista1 if x % 7 == 0]
print("ex1:", lista_1)

# ex2 Find all of the numbers from 1-50 that have a 3 in them.
lista2 = [i for i in range(1, 50)]
lista_2 = [x for x in lista2 if "3" in str(x)]
print("ex2:", lista_2)

# ex3 Create a list of all the consonants in the string “Yellow Yaks like yelling and yawning and yesterday they
# yodeled while eating yucky yams”

s = "Yellow Yaks like yelling and yawning and yesterday they yodeled while eating yucky yams"
lista3 = s.split()
lista_3 = [i for j in range(len(lista3)) for i in lista3[j] if i in "aeyuio"]
print("ex3:", lista_3)

# ex4 Find the common numbers in two lists (without using a tuple or set).

lista_a = [1, 2, 3, 4, 9, 13, 27]
lista_b = [2, 3, 4, 5, 27, 13]
lista4 = [i for i in lista_a if i in lista_b]
print("ex4:", lista4)

# ex5 Get only the numbers in a sentence like ‘In 1984 there were 13 instances of a protest with over 1000 people
# attending’.

s = "In 1984 there were 13 instances of a protest with over 1000 people attending"
lista5 = s.split()
lista_5 = [int(i) for i in lista5 if i.isdigit() == True]
print("ex5:", lista_5)

# ex6 На вход программе подается строка текста, содержащая целые числа. Напишите программу, использующую списочное
# выражение, которая выведет квадраты четных чисел, которые не оканчиваются на цифру 4.

from math import*
s = input().split()
lista = [pow(int(i), 2) for i in s if pow(int(i), 2) % 10 != 4 and int(i) % 2 == 0]
print("ex6:", *lista)

