import socket

some_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM) # -create socket
some_socket.bind(('127.0.0.1', 5000)) # bind ip address and port to socket
some_socket.listen(2) # method listen(n) is used to say that socket is active and can receive internet  connection,
# n - discribe how many request socket can receive


print('Working ... ')

client_socket, address = some_socket.accept() #method accept() is used to say that socket is waiting to request from clients
data = client_socket.recv(1024).decode('utf-8') #recv() (receive) method is used to read data which was send by cliend
print(data)

HDRS = 'HTTP/1.1 200 OK\r\nContent-Type: text/html; charset=utf-8\r\n\r\n'
content = 'Well done, buddy...'.encode('utf-8')
client_socket.send(HDRS.encode('utf-8') + content) #method send() is used to send data to client
client_socket.close() # close() is used to close connection with client


# AF_INET - IPv4
# AF_INET6 - IPv6
# SOCK_STREAM - TCP
# SOCK_DGRAM - UDP
# connect() - method connect is used by clients to connect with server as a params you should write server ip address
# and server port