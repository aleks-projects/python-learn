class Students:
    def __init__(self, name, marks):
        self.name = name
        self.marks = list(marks)

    def __getitem__(self, item):
        if 0 <= item < len(self.marks):
            return self.marks[item]
        else:
            raise IndexError("Uncorrected Index")

    def __setitem__(self, key, value):
        if isinstance(key, int) and key >= 0:
            num = key + 1 - len(self.marks)
            self.marks.extend([None] * num)
            self.marks[key] = value
        else:
            raise TypeError("Keys must be integer greater or equal zero")

    def __delitem__(self, key):
        if isinstance(key, int) and 0 <= key < len(self.marks):
            del self.marks[key]
        else:
            raise TypeError("Keys must be integer greater or equal zero and smaller than list lengths ")


s = Students("Alex", [2, 5, 6, 6, 4, 6, 7])
del s[3]

print(s.__dict__)
