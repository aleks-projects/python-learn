class Geom:
    def get_pr(self):
        raise NotImplementedError(f"Not found get_pr method{self.__class__}")


class Rectangle(Geom):
    def __init__(self, a, b):
        self.b = b
        self.a = a

    def get_pr(self):
        return 2 * (self.a + self.b)


class Square(Geom):
    def __init__(self, a):
        self.a = a

    def get_pr(self):
        return 4 * self.a


class Triangle(Geom):
    def __init__(self, a, b, c):
        self.a = a
        self.b = b
        self.c = c

    # def get_pr(self):
    #     return self.a + self.b + self.c


geom = [Rectangle(1, 2), Rectangle(3, 4),
        Square(10), Square(20),
        Triangle(3, 4, 5), Triangle(5, 6, 7)
        ]
for g in geom:
    print(g.get_pr())
