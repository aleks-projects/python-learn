class Geom:
    name = "Geom"

    def __init__(self, x1, y1, x2, y2):
        print(f"Geom init for {self.__class__}")
        self.x1 = x1
        self.y1 = y1
        self.x2 = x2
        self.y2 = y2


class Line(Geom):

    def draw(self):
        print("Draw line")


class Rect(Geom):

    def __init__(self, x1, y1, x2, y2, fill=None):
        super().__init__(x1, y1, x2, y2)
        print("Rect init")
        self.fill = fill

    def draw(self):
        print("Draw rect")


l = Line(1, 1, 2, 2)
r = Rect(1, 1, 2, 2, "red")
print(r.__dict__)

