class Vector:
    min_coord = 0
    maks_coord = 100

    @classmethod     # rabotajet tolko z atributami klasa, nie mozet obraszczatsia k lokalnym atriutam egzemplara klasa
    def validate(cls, arg):
        return cls.min_coord <= arg <= cls.maks_coord

    def __init__(self, x, y):
        self.x = self.y = 0
        if self.validate(x) and self.validate(y):
            self.x = x
            self.y = y
        print(self.norm2(self.x, self.y))

    def get_coord(self):
        return self.x, self.y

    @staticmethod    # nie imiejet dostupa nie k atributam klasa nie k atributam egzemplarow nikaja niezawisimoja funkcija
    def norm2(x, y):
        return x * x + y * y

v = Vector(2, 5)
print(Vector.validate(4))
print(Vector.norm2(5, 6))
print(v.get_coord())