from math import *


class Counter:
    def __init__(self):
        self.__counter = 0

    def __call__(self, step=1, *args, **kwargs):
        print("method __call__")
        self.__counter += step
        return self.__counter


c = Counter()
c2 = Counter()
c(2)
c(2)
res = c(3)
res2 = c2(4)
print(res, res2)


class Dirivate:
    def __init__(self, func):
        self.__func = func

    def __call__(self, x, dx=0.0001, *args, **kwargs):
        return (self.__func(x + dx) - self.__func(x)) / dx


def df_sin(x):
    return sin(x)


df_sin = Dirivate(df_sin)
print(df_sin(pi / 4))
