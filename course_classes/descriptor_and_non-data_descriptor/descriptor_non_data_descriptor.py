# star data descriptor
class Integer:
    def __set_name__(self, owner, name):
        self.name = "_" + name

    def __get__(self, instance, owner):
        getattr()

    def __set__(self, instance, value):
        print(f"__set__:{self.name}= {value}")
        self.check_correct_value(value)
        setattr(instance, self.name, value)

    @classmethod
    def check_correct_value(cls, x):
        if type(x) != int:
            raise TypeError("Value must be integer digit")


# stop data descriptor

class Point3D:
    x = Integer()
    y = Integer()
    z = Integer()

    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z


p1 = Point3D(2, 3, 8)
print(p1.__dict__)
