class Geom:
    pass


class Line(Geom):
    pass


g = Geom()
l = Line()
print(Geom)
print("1:", issubclass(Line, Geom))
print("2:", issubclass(Line, object))
print("3:", (issubclass(Geom, Line)))
print("4:", (issubclass(object, object)))
print("5:", (issubclass(Line, Line)))
print("6:", isinstance(l, Line))
print("7:", isinstance(g, Line))
print("8:", isinstance(l, object))
print("9:", isinstance(Geom, object))


class Vector(list):
    def __str__(self):
        return " ".join(map(str, self))


v = Vector([1, 2, 3, 4])
print(v.__dict__)
print(v)
print(type(v))
