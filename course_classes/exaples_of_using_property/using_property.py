# passport data must be xxxx xxxxxx, where x is digit.
from string import ascii_letters


class Person:
    S_RUS = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя-"
    S_RUS_UPPER = S_RUS.upper()

    def __init__(self, fio, old, weight, ps):
        self.check_correct_fio(fio)
        self.check_old(old)
        self.check_weight(weight)
        self.check_passport_data(ps)
        self.__fio = fio.split()
        self.__old = old
        self.__weight = weight
        self.__passport = ps

    @classmethod
    def check_correct_fio(cls, fio):
        if type(fio) != str:
            raise TypeError("fio must be string")
        f = fio.split()
        if len(f) != 3:
            raise TypeError("Not correct format")
        letters = ascii_letters + cls.S_RUS + cls.S_RUS_UPPER
        for s in f:
            if len(s) == 0:
                raise TypeError("In fio must be at least one symbol ")
            if len(s.strip(letters)) != 0:
                raise TypeError("in fio are incorrect symbols")

    @classmethod
    def check_old(cls, old):
        if type(old) != int:
            raise TypeError("Old must be integer digit")
        if old < 14 or old > 120:
            raise TypeError("Incorrect old")

    @classmethod
    def check_weight(cls, w):
        if type(w) != int and type(w) != float:
            raise TypeError("Weight must be integer or float digit")
        if w < 20:
            raise TypeError("Incorrect weight")

    @classmethod
    def check_passport_data(cls, p):
        if type(p) != str:
            raise TypeError("Passport data must be str")
        s = p.split()
        if len(s) != 2 or len(s[0]) != 4 or len(s[1]) != 6:
            raise TypeError("Format of passport data is incorrect")
        for i in s:
            if not i.isdigit():
                raise TypeError("Symbols in passport data must be digits")
    @property
    def fio(self):
        return self.__fio
    @property
    def old(self):
        return self.__old
    @old.setter
    def old(self, old):
        self.check_old(old)
        self.__old = old

    @property
    def weight(self):
        return self.__weight

    @weight.setter
    def weight(self, weight):
        self.check_weight(weight)
        self.__weight = weight

    @property
    def passport(self):

        return self.__passport

    @passport.setter
    def passport(self, ps):
        self.check_passport_data(ps)
        self.__passport = ps


p1 = Person("John Swith Wersow", 19, 22, "4234 131413")
p1.old = 43
p1.weight = 95
p1.passport = "1234 123456"
print(p1.__dict__)