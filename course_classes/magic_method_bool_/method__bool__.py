class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __len__(self):
        print("len")
        return self.x * self.x + self.y * self.y

    def __bool__(self):
        print("bool")
        return self.x == self.y


p1 = Point(0, 5)
if p1:
    print("Object p returns True")
else:
    print("Object p returns False")

