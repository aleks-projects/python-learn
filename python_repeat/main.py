from datetime import datetime


# ------types of data in python
# string = "aac"
# integer = 5
# float = 4.3
# booly = True# lista = [1,"af"]
# dictionary = {"name": "aleks", 5.0: "14"}
# tuple = ("aerwq", 1235)
# print(dictionary)
# seet = {"adf", 'adf', "a", "b", "c", "d", "e"} - tolko unikalnyje, znaczienija
# seet.add("debil")
#

#restful api - арзиьектура которая описывает как будет идти общение client server. У restful api усть 4 метода
# get post update i delete.
#api interfiejs kotoryj pozwoliajet poluchic dannyje dla swojego prilozenija.


#--------------list comprehension and dictionaries
# lista_2 = [[[k for k in range(0,4)] for j in range(0,3)] for i in range(0,4)]
# print(lista_2)
#
# my_dict = {"name":["aleks", "siemionow", {"tolia": ("pdor", "czesc")}]}

# -------------functions
#def my_func(lista: list) -> list:
#    return sorted(lista)
#
#
# print(my_func([1,4, 5, 6, 7, 4, 24, 1, 3]))
# print(my_func('a'))
#
# def sum(a,b,c):#
# return a+b#
#
# def func_1(*args, **kwargs):
#    suma = sum(args)
#    for i in kwargs.values():
#        suma += i
#    return suma###
#
# print(func_1(1,2,3,a=4,b=5,c=6))#
# lista = [2 for i in range(0,30)]#
# print(lista)#
# print([2] * 30 )

# --------classes
# class Person:
#     name = "Python"
#
#     def __init__(self, name, surename, age, mail, a, phones):
#         self.name = name
#         self.surename = surename
#         self.age = age
#         self.mail = mail
#         self.a = a
#         self.phones = phones
#
#     def __add__(self, other):
#         return self.a + other.a
#
#     def get_fio(self):
#          return self.name + " " + self.surename
#
#     @staticmethod
#     def metoda(a, b):
#         return a+b
#     @classmethod
#     def print_general_name(cls):
#         return cls.name
#     @property
#     def age(self):
#         return self.__age
#     @age.setter
#     def age(self, age):
#         if age < 0:
#             raise ValueError("Must be positive integer")
#         else:
#             self.__age = age
#
#     @property
#     def mail(self):
#         return self.mail
#
#     @mail.setter
#     def mail(self, mail):
#         if "@" not in mail:
#             raise ValueError("E-mail must has @")
#         else:
#             self.__mail = mail
# # iterators,__iter__ __next__ methods
#
#     def __iter__(self):
#         self.objects = [self.name, self.surename, self.__mail, self.phones, self.__age, self.a]
#         self.index = -1
#         return self
#
#     def __next__(self):
#         if len(self.objects) != self.index+1:
#             self.index += 1
#             return self.objects[self.index]
#         else:
#             raise StopIteration


# alex = Person("Alex", "Siemionow", 5, "1er134@gmail.com", 14,[1,2,3])
# print(alex.__dict__)
# print(alex.get_fio())
# print(alex.metoda(3 ,4))
# print(alex.print_general_name())
# alex.age = 1# print(alex.__dict__)
# ---- __iter__ and __next__ methods iterations
# for i in alex:
#     print(i)


class Numbers:

    def __init__(self, num):
        self.num = num

    @property
    def num(self):
        return self.__num

    @num.setter
    def num(self, x):
        self.__num = x


n_1 = Numbers(5)
n_1.num = 10
print(n_1.__dict__)

#     def __iter__(self):
#         self.a = -2
#         return self
#
#     def __next__(self):
#         if self.a != self.__num:
#             self.a += 2
#             return self.a
#         else:
#             raise StopIteration
#
#
#
#
n_1 = Numbers(20)
print(n_1.num)

# for i in n_1:
#     print(i)


#-------generators
# def funkcja(a):
#     counter = 0
#     while counter != a+2:
#         yield counter
#         counter += 2
#
#
# for i in funkcja(20):
#     print(i)
#
# a = (i for i in range(3))
# print(next(a))
# print(next(a))
# print(next(a))


# -----decorators posmotriet video
# def print_string_reversed(func):
#     print(1)
#     def wrapper(text):
#         print(text)
#         a = func(text)
#         return a[::-1] * 2
#     return wrapper
#
#
# @print_string_reversed
# def print_string(text: str) -> str:
#     return text
#
#
#
#
# print(print_string("czesc"))

# magic methods __str__ and __call__
# class File:
#
#     def __init__(self, file_name, type):
#         self.file_name = open(file_name, type)
#         self.type = type
#
#     def __enter__(self):
#         return self.file_name
# #
#     def __exit__(self, exc_type, exc_val, exc_tb):
#        if exc_type is not None:
#            print("Error")
#            self.file_name.close()
#        return False
# #
# #
# #
# file = File("plik.txt", "w")
#
# with file as f:
#     f.write("kakdiela")
#     raise EOFError
#
# print("HI")

# contex manager reppeat

# def decor(*args):
#     def wraper_1(func):
#         print(args)
#         def wrapper_2(name):
#             print("Decora start")
#             print("Decora stop")
#             return func(name)
#         return wrapper_2
#     return wraper_1
#
#
#
# @decor('priviet')
# def printi(name):
#     return "Hello world", name
#
#
# print(printi("Alex"))

# magic method __new__


# rekurenc
lista = ["a", ["b", "d", ["e", "f"], "g"], "h"]


# napisac rekurencju tak sztoby kazduju wlozanuju listu on piechatal s nowymi 5 otstupami
# output :
# a
#     b
#     d
#          e
#          f
#     g
# h

# def func_rec(lista: list, n: int):
#     my_list = []
#     ans_list = []
#     if all(isinstance(el, str) for el in lista):
#         lista = [n*" "+x for x in lista]
#         return lista
#     for i in lista:
#         if isinstance(i, str):
#             ans_list.append(n*" "+i)
#         else:
#             my_list = i
#     return ans_list + func_rec(my_list, n+5)
#
#
# def func_rec_2(lista, n):
#     for i in lista:
#         if isinstance(i, str):
#             print(n*" "+ i)
#         else:
#             func_rec_2(i, n+5)
# #
# #
# func_rec_2(lista, 5 )
#
# print(func_rec(lista,0))
#
# def funcio(n):
#     if n == 1:
#         return 0
#     elif n == 2:
#         return 1
#     else:
#         return funcio(n-1) + funcio(n-2)
#
# print(funcio(8))
# def funkcja(lista: list):
#     if len(lista) == 0:
#         return "List is emty"
#     else:
#         ans = None
#         counter = 0
#         my_list = []
#         for i in lista:
#             if lista.count(i) > counter:
#                 counter = lista.count(i)
#                 ans = i
#             elif lista.count(i) == counter:
#                 my_list.append(ans)
#                 ans = my_list
#                 ans.append(i)
#         return ans


# print(funkcja([1, 2, 2, 3, 3]))

# list = [10, 8, 2, 6, 3, 4, 8, 5]
# def func_1(lista, n):
#     a = n
#     lista = sorted(lista, reverse=True)
#     counter = 0
#     for i in range(len(lista)):
#         if lista[i] == n:
#             return 1
#         elif lista[i] < n:
#             lista = lista [i:]
#             break
#     while lista:
#         for i in lista:
#             n = n - i
#             counter += 1
#             if n < 0:
#                 del lista[0]
#                 counter = 0
#                 n = a
#                 break
#             elif n == 0:
#                 return counter
#             else:
#                 continue
#     return -1
#
# print(func_1(list, 1))

# def func_1()


# def polindrome(text):
#     if len(text) == 0:
#         return True
#     else:
#         if text[0] == text[len(text)-1]:
#             text = text[1:len(text)-1]
#             return bool(True + polindrome(text))
#         else:
#             return False
#
# print(polindrome("alenela"))



class Point:

    def __init__(self,a,b):
        self.__old = a
        self.b = b

    @property
    def old(self):
        return self.__old

    @old.setter
    def old(self, a):
        if a < 0:
            raise ValueError("ERROr")
        self.__old = a

p_1 = Point(1,2)
print(p_1.old)
p_1.old = 134
print(p_1.old)


for i in range(-5,-1):
    print(i)