#Given two strings needle and haystack, return the index of the first occurrence of needle in haystack,
# or -1 if needle is not part of haystack

def strStr(haystack: str, needle: str) -> int:
    if needle not in haystack:
        return -1
    else:
        return haystack.index(needle)