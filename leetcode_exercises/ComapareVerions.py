def compareVersion(version1: str, version2: str) -> int:
    version1, version2 = list(map(int,version1.split('.'))), list(map(int,version2.split('.')))

    if len(version1) > len(version2):
        version2 += [0]*(len(version1)-len(version2))
    elif len(version2) > len(version1):
        version1 += [0] * (len(version2) - len(version1))

    for i in range(len(version1)):
        if version1[i] > version2[i]:
            return 1
        elif version1[i] < version2[i]:
            return -1
    return 0





print(compareVersion("1.0","1.0.0"))