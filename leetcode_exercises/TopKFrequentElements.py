

def topKFrequent(nums: list[int], k: int) -> list[int]:
    counts = [0] * k
    dicti = dict()
    unique = list(set(nums))
    for i in unique:
        ct = nums.count(i)
        dicti[i] = ct
        if ct > min(counts):
            idx = counts.index(min(counts))
            del counts[idx]
            counts.append(ct)
    return [x for x in dicti if dicti[x] in counts]


print(topKFrequent([4,1,-1,2,-1,2,3],2))

nums = [1,1,1]
player_1 = [nums[x] for x in range(len(nums)) if x%2 ==0 ]
player_2 = sum([nums[x] for x in range(len(nums)) if x%2 !=0 ])
print(player_1, player_2)