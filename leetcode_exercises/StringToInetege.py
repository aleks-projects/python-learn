
def myAtoi(s: str) -> int:
    counter_1 = 0
    counter_2 = 0
    if not s or s[0] not in '1234567890-+ ':
        return 0
    my_num = []
    for i in s:
        if i.isdigit() or i == '-' or i == "+" or i == ".":
            if i in "-+":
                counter_2 +=1
            if counter_2 > 1:
                break
            counter_1 += 1
            my_num.append(i)
        elif i == " ":
            if counter_1 != 0:
                break
            else:
                continue
        else:
            break
    my_num = ''.join(my_num)
    if my_num[len(my_num)-1] in '+-':
        return int(my_num[:len(my_num)-1])
    if (len(my_num) == 1 and not my_num.isdigit()) or (len(my_num) == 2 and my_num[0] in "+-" and my_num[1] in "+-"):
        return 0
    else:
        try:
            my_num = int(round(float(my_num)))
        except:
            return 0
    if my_num > 2 ** 31 - 1:
        return 2 ** 31 - 1
    elif my_num < -2 ** 31:
        return -2 ** 31
    else:
        return my_num

print(myAtoi( "+-2"))