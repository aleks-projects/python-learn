def coinChange(coins: list[int], amount: int) -> int:
    ans = -1
    if amount < min(coins):
        return ans
    coins.sort(reverse=True)
    for i in range(len(coins)):
        if coins[i] == amount:
            return 1
        elif coins[i] < amount:
            coins[:]= coins[i:]
            break
    while amount != 0:
        for i in coins:
            if amount % i == 0:
                return amount // i



print(coinChange([1,2,5,12,16],15))