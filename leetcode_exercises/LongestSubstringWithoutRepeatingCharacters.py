#Given a string s, find the length of the longest substring without repeating characters.

def lengthOfLongestSubstring(s: str) -> int:
    substring_1 = ''
    substring_2 = ''
    flag = True
    if s == "":
        return 0
    while flag:
        if len(s) == 0:
            return len(substring_2)
        for i in range(len(s)):
            if s[i] in substring_1:
                if len(substring_2) < len(substring_1):
                    substring_2, substring_1 = substring_1, ''
                else:
                    substring_1 = ''
                s = s[1:]
                break
            else:
                substring_1 += s[i]
