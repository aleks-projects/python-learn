

def twoSum(numbers: list[int], target: int) -> list[int]:
    numbers.sort()
    for i in range(len(numbers)):
        left = i
        right = len(numbers)-1

        while left < right:
            if numbers[left] + numbers[right] == target:
                return [left, right]
            elif numbers[left] + numbers[right] < target:
                left += 1
            elif numbers[left] + numbers[right] > target:
                right -= 1
    return -1
print(twoSum([2,3,7,11],9))