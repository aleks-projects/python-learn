# You are given an array prices where prices[i] is the price of a given stock on the ith day.
#
# You want to maximize your profit by choosing a single day to buy one stock and choosing a different day in the future to sell that stock.
#
# Return the maximum profit you can achieve from this transaction. If you cannot achieve any profit, return 0.
#

def maxProfit(prices: list[int]) -> int:
    profit = 0
    min_buy = prices[0]
    for i in range(len(prices)-1):
        if prices[i] < min_buy:
            min_buy = prices[i]
        if prices[i+1] - min_buy > profit:
            profit = prices[i+1] - min_buy
    return profit
print(maxProfit([7,6,4,3,1]))