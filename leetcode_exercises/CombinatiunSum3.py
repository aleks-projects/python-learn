def combinationSum3(k: int, n: int) -> list[list[int]]:
    if k > n:
        return []

    digits = [i for i in range(1, n + 1)]

    all_combines = [[]]
    for i in digits:
        all_combines += [[i] + j for j in all_combines]

    return [x for x in all_combines if len(x)==k and sum(x)==n]



print(combinationSum3(3,7))