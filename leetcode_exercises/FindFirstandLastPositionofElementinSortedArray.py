# Given an array of integers nums sorted in non-decreasing order, find the starting and ending position of a given target value.
#
# If target is not found in the array, return [-1, -1].
#
# You must write an algorithm with O(log n) runtime complexity.

# Example 1:
#
# Input: nums = [5,7,7,8,8,10], target = 8
# Output: [3,4]
# Example 2:
#
# Input: nums = [5,7,7,8,8,10], target = 6
# Output: [-1,-1]


def searchRange(nums: list[int], target: int) -> list[int]:
    ans = []
    flag = False
    if not nums or nums[0] > target:
        return [-1, -1]
    else:
        for i in range(len(nums)):
            if nums[i] < target:
                continue
            elif nums[i] == target:
                flag = True
                ans.append(i)
            elif nums[i] > target:
                if flag:
                    return [ans[0], ans[-1]]
    if not ans:
        return [-1,-1]
    return [ans[0], ans[-1]]
print(searchRange([2,2,5,5,5,6],5))


def searchRange2(nums: list[int], target: int) -> list[int]:
    ans = [-1,-1]
    if not nums or nums[0] > target:
        return [-1, -1]
    else:
        left = 0
        right = len(nums)-1
        while nums[left] != target:
            if left == len(nums)-1 or nums[left] > target:
                return ans
            if nums[left] < target:
                left += 1
        del ans[0]
        ans.insert(0, left)
        while nums[right] != target:
            if  nums[right] > target:
                right -= 1
        del ans[-1]
        ans.append(right)
    return ans
print(searchRange2([2,2,3,3,3,4],3))