# Given an integer x, return true if x is a palindrome and false otherwise.

def isPalindrome(x: int) -> bool:
    if str(x) == str(x)[::-1]:
        return True
    else:
        return False