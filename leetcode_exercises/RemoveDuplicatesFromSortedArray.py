# Given an integer array nums sorted in non-decreasing order, remove the duplicates in-place such that each unique element appears only once.
# The relative order of the elements should be kept the same. Then return the number of unique elements in nums.


def removeDuplicates(nums: list[int]) -> int:
    nums = sorted(list(set(nums)))
    return len(nums)

print(removeDuplicates([0,0,1,1,1,2,2,3,3,4]))