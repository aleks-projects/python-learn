strin = "    the sky is blue  "
print(' '.join(strin.strip().split()[::-1]))

def groupAnagrams(strs: list[str]) -> list[list[str]]:
    kopia = strs.copy()
    for idx, stri in enumerate(kopia):
        kopia[idx] = ''.join(sorted(stri))
    indexes = [[i for i in range(len(kopia)) if kopia[i]== j] for j in set(kopia)]
    ans = [[strs[j] for j in i] for i in indexes]

    return ans




print(groupAnagrams(["eat","tea","tan","ate","nat","bat"]))


def countPrimes(n: int) -> int:
    if n <= 2:
        return 0
    primes = [2]
    for i in range(3, n):
        flag = True
        for j in range(2, i):
            if i % j == 0:
                flag = False
                break
        if flag:
            primes.append(i)
    print(primes)
    return len(primes)
print(countPrimes(10))