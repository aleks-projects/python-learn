from collections import defaultdict


def longestSubsequence(arr: list[int], difference: int) -> int:
    d = defaultdict(int)
    for n in arr:
        d[n] = d[n - difference] + 1
    return max(d.values())


print(longestSubsequence([2,1,3,4,1,5,2,1,3,4,5,6], 1))




