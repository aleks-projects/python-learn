def climbingLeaderboard(ranked:list[int], player:list[int]) -> list[int]:
    ranked = sorted(set(ranked),reverse=True)
    ans = []
    right = len(ranked)-1
    for i in player:
        while len(ans) != len(player):
            if i > ranked[0]:
                ans.append(1)
            if i < ranked[right]:
                ans.append(right+2)
                # ranked.insert(right+1,i)
                break
            elif i> ranked[right]:
                right -= 1
            elif i == ranked[right]:
                ans.append(right+1)
                break
    return ans
print(climbingLeaderboard([100, 100, 50, 40, 40, 20, 10], [5, 25, 50, 120]))