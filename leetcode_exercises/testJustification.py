def maximumBeauty(nums: list[int], k: int) -> int:
    maxim = 1
    nums.sort()
    for i in range(len(nums)):
        counter = 1
        for j in range(i+1, len(nums)):
            if nums[i]+ k > nums[j] or  nums[i]+ k >= nums[j] - k:
                counter +=1
        maxim = max(counter, maxim)
        if maxim > len(nums[i:]):
            break
    return maxim


print(maximumBeauty([1,2,14,30,47,81,85],23))






