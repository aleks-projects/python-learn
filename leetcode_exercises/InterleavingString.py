def isInterleave(s1: str, s2: str, s3: str) -> bool:

    flag = True
    while flag:
        counter = 0
        counter_2 = 0
        while len(s1)-1 >= counter and len(s3)-1 >= counter and s1[counter] == s3[counter]:
            counter += 1
            counter_2 +=1
        s3 = s3[counter:]
        s1 = s1[counter:]
        counter = 0
        while len(s2)-1 >= counter and len(s3)-1 >= counter  and s2[counter] == s3[counter]:
            counter_2 += 1
            counter +=1
        s3 = s3[counter:]
        s2 = s2[counter:]
        if s3 == '' and s2 == "" and s1=="":
            return True
        elif counter_2 ==0:
            return False

print(isInterleave("aabcc","b","a"))
def minimum_number(numbers):
    actual_sum = sum(numbers)
    digit = -1
    flag = True
    while flag:
        digit += 1
        flag_2 = True
        for i in range(2, actual_sum + digit):
            if (actual_sum+digit) % i == 0:
                flag_2 = False
                break
        if flag_2:
            flag = False
    return digit

print(minimum_number([3,1,2]))