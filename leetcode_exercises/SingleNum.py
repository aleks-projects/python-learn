# Given a non-empty array of integers nums, every element appears twice except for one. Find that single one.
#
#
#
# You must implement a solution with a linear runtime complexity and use only constant extra space.


def singleNumber(nums: list[int]) -> int:
    nums.sort()
    for i in range(1,len(nums)-1):
        if nums[i] != nums[i-1] and nums[i] != nums[i+1]:
            return nums[i]
    if nums.count(nums[0]) == 1:
        return nums[0]
    return nums[-1]


print(singleNumber([2,2,3,4,3,5,4]))


def singleNumber_2(nums: list[int]) -> int:
    for i in list(set(nums)):
        if nums.count(i) == 1:
            return i

print(singleNumber_2([4,1,2,1,2]))


def singleNumber_3(nums: list[int]) -> int:
    return [i for i in list(set(nums)) if nums.count(i) == 1][0]
