# Given an integer n, count the total number of digit 1 appearing in all non-negative integers less than or equal to n.




def countDigitOne(n: int) -> int:
    return sum([str(i).count("1") for i in range(n+1)])

print(countDigitOne(824883294))
