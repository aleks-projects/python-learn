def reverse(x: int) -> int:
    if str(x)[0] == "-":
        return 0 - int(str(x)[:0:-1])
    else:
        return int(str(x)[::-1])


print(reverse(-123))