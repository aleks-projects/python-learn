#Given an unsorted integer array nums, return the smallest missing positive integer.



def firstMissingPositive(nums: list[int]) -> int:
    v = sorted(list(set(nums)))
    flag = True
    if v[0] <= 0 or v[0] == 1:
        for j in v:
            if j > 0:
                v = v[v.index(j):]
                flag = False
                break
    if flag:
        return 1
    lenght = len(v)
    for i in range(len(v)):
        if v[0] > 0 and v[0] != 1:
            return 1
        elif i == lenght - 1:
            return v[i] + 1
        elif v[i] > 0 and v[i] + 1 != v[i + 1]:
            return v[i] + 1