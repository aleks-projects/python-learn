# You are given an integer array prices where prices[i] is the price of a given stock on the ith day.
#
# On each day, you may decide to buy and/or sell the stock. You can only hold at most one share of the stock at any time.
# However, you can buy it then immediately sell it on the same day.
#
# Find and return the maximum profit you can achieve.


def maxProfit(prices: list[int]) -> int:
    min_price = prices[0]
    profit = 0
    counter = 0
    while counter <= len(prices) -1:
        if counter == len(prices)-1:
            return profit
        if prices[counter] <= min_price:
            min_price = prices[counter]
            flag = True
            counter += 1
            while flag:
                if min_price > prices[counter]:
                    min_price = prices[counter]
                    counter +=1
                    if counter >= len(prices):
                        return profit
                else:
                    max_price = prices[counter]
                    while flag:
                        if max_price <= prices[counter]:
                            max_price = prices[counter]
                            counter += 1
                            if counter >= len(prices):
                                profit += max_price - min_price
                                return profit
                        else:
                            profit += max_price - min_price
                            min_price = prices[counter]
                            flag = False
        else:
            counter += 1
print(maxProfit([1,2,28,5,6,12,13,4,3,18]))


def maxProfit_1(prices: list[int]) -> int:
    price = 0
    for i in range(1, len(prices)):
        if prices[i - 1] < prices[i]:
            price += prices[i] - prices[i - 1]
    return price

print(maxProfit_1([1,2,28,5,6,12,13,4,3,18]))