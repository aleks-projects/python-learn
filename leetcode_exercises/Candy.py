# There are n children standing in a line. Each child is assigned a rating value given in the integer array ratings.
#
# You are giving candies to these children subjected to the following requirements:
#
# Each child must have at least one candy.
# Children with a higher rating get more candies than their neighbors.
# Return the minimum number of candies you need to have to distribute the candies to the children.


def candy(ratings: list[int]) -> int:
    n = len(ratings)
    give = [1 for i in ratings]

    for i in range(1, n):
        if ratings[i] > ratings[i - 1]:
            give[i] = give[i - 1] + 1

    for i in range(n - 2, -1, -1):
        if ratings[i] > ratings[i + 1] and give[i] <= give[i + 1]:
            give[i] = give[i + 1] + 1

    return sum(give)

print(candy([1,2,3,4,4,3,1,2]))