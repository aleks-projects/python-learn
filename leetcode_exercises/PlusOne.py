


def plusOne(digits) :
    if digits[-1] != 9:
        digits[-1] += 1
        return digits
    else:
        return list(map(int,list(str(int("".join(map(str,digits)))+1))))


print(plusOne([9,9]))
