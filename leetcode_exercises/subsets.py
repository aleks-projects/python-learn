

def subsets(nums: list[int]) -> list[list[int]]:
    ans = [[]]
    for j in nums:
        ans += [i + [j] for i in ans]
    return ans


print(subsets([7,8,5,3,1]))

