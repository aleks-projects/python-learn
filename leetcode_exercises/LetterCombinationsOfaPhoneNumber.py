# Given a string containing digits from 2-9 inclusive, return all possible letter combinations that the number could represent.
# Return the answer in any order.
#
# A mapping of digits to letters (just like on the telephone buttons) is given below. Note that 1 does not map to any letters.


def letterCombinations(digits: str):
    my_dict = {
        '2':'abc', '3':'def', '4':'ghi', '5':'jkl', '6':'mno', '7':'pqrs', '8':'tuv', '9':'wxyz'
    }
    if len(digits) == 0:
        return []
    elif len(digits) == 1:
        return [i for i in my_dict[digits]]
    elif len(digits) == 2:
        return [i+j for i in my_dict[digits[0]] for j in my_dict[digits[1]]]
    elif len(digits) == 3:
        return [i + j +k for i in my_dict[digits[0]] for j in my_dict[digits[1]] for k in my_dict[digits[2]]]
    else:
        return [i + j +k +b for i in my_dict[digits[0]] for j in my_dict[digits[1]] for k in my_dict[digits[2]] for b in my_dict[digits[3]]]



print(letterCombinations("5678"))