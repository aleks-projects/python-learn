#Given a roman numeral, convert it to an integer.
#Symbol       Value
# I             1
# V             5
# X             10
# L             50
# C             100
# D             500
# M             1000


def romanToInt(s: str) -> int:
    num = 0
    lista = []
    for i in range(len(s)):
        if s[i] == "I":
            digit = 1
        elif s[i] == "V":
            digit = 5
        elif s[i] == "X":
            digit = 10
        elif s[i] == "L":
            digit = 50
        elif s[i] == "C":
            digit = 100
        elif s[i] == "D":
            digit = 500
        else:
            digit = 1000
        lista.append(digit)
    for i in range(len(lista)):
        if i == len(lista) - 1:
            num += lista[i]
        elif lista[i] < lista[i + 1]:
            num -= lista[i]
        else:
            num += lista[i]
    return num