# def climbStairs(n: int) -> int:
#     if n == 1:
#         return 1
#     elif n == 0:
#         return 1
#     else:
#         return climbStairs(n - 1) + climbStairs(n - 2)
#
#
# print(climbStairs(40))



def climbinStairs2(n:int) -> int:
    if n <=2:
        return n
    p1 = 1
    p2 = 2
    ans = 0
    for i in range(2,n):
        ans = p1+p2
        p1,p2=p2,ans
    return ans

print(climbinStairs2(10))