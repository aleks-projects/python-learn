import time


def decor(func):
    def wrapper(nums):
        start = time.time()
        ans = func(nums)
        end = time.time()
        return ans, end - start
    return wrapper


@decor
def longestConsecutive(nums: list[int]) -> int:
    nums = sorted(list(set(nums)))
    max_len = 1
    while nums and len(nums)!=1:
        curr_lenght = 1
        for i in range(len(nums) - 1):
            if nums[i] + 1 == nums[i + 1]:
                curr_lenght += 1
            else:
                break
        max_len = max(curr_lenght, max_len)
        nums = nums[curr_lenght:]
    return max_len



print(longestConsecutive([0,3,7,2,5,8,4,6,0,1,-1,2,3,4,5,36,3,2,3,41,3,235,235,236,3,75,32,1,2,3,4,14,-14,-14,-14,-344,124,1,3,4,5,6,7,-2,3,4,5,6,7,7,8,9,0,-3,1,2,3,4,5,6,7,88,4,1,2,3]))