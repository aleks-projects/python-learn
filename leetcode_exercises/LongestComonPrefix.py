#Write a function to find the longest common prefix string amongst an array of strings.
#If there is no common prefix, return an empty string "".


def longestCommonPrefix(v: list[str]) -> str:
    ans = ""
    v = sorted(v)
    print(v)
    first = v[0]
    last = v[-1]
    for i in range(min(len(first), len(last))):
        if (first[i] != last[i]):
            return ans
        ans += first[i]
    return ans