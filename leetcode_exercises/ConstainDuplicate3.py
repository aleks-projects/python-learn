



def containsNearbyAlmostDuplicate(nums: list[int], indexDiff: int, valueDiff: int) -> bool:
    count_cond = 0
    for i in range(len(nums)):
        if i+indexDiff > len(nums):
            loop = i+len(nums[i:])
        elif i+indexDiff == len(nums):
            loop = i+indexDiff
        else:
            loop = i+indexDiff+1
        for j in range(i+1, loop):
            if abs(i-j) <= indexDiff:
                count_cond += 1
            if abs(nums[i] - nums[j]) <= valueDiff:
                count_cond += 1
            if count_cond == 2:
                print(nums[i], nums[j])
                return True
            count_cond = 0

    return False


print(containsNearbyAlmostDuplicate([2433,3054,9952,6470],  1000,0))