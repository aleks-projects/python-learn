#Given two sorted arrays nums1 and nums2 of size m and n respectively, return the median of the two sorted arrays.
#The overall run time complexity should be O(log (m+n))


def findMedianSortedArrays(nums1: list[int], nums2: list[int]) -> float:
    my_list = sorted(nums1 + nums2)
    if len(my_list) % 2 != 0:
        return float(my_list[int(len(my_list)/2)])
    else:
        return float((my_list[int(len(my_list)/2)]+my_list[int(len(my_list)/2)-1])/2)


print(findMedianSortedArrays([1,2],[3,4]))

