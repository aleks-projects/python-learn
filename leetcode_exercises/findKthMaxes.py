import re
# def findKthLargest( nums: list[int], k: int) -> int:
#     maxes = nums[:k]
#     nums = nums[k:]
#     for i in nums:
#         curr_min = min(maxes)
#         if i > curr_min:
#             del maxes[maxes.index(curr_min)]
#             maxes.append(i)
#     return min(maxes)
#
#
# print(findKthLargest([6,3,2,1,5,4],2))

import re


class WordDictionary:

    def __init__(self, obj=[]):
        self.obj = []
    def addWord(self, word: str) -> None:
        self.obj.append(word)

    def search(self, word: str) -> bool:
        regex = re.compile(re.sub(r'\.', '[a-z]{1}', word))
        return bool(regex.findall(' '.join(self.obj)))


wordDict = WordDictionary()
print(wordDict.search('a'))

