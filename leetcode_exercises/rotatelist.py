def rotate(nums: list[int], k: int) -> None:
    """
    Do not return anything, modify nums in-place instead.
    """
    if len(nums) < 2 or k == 0:
        nums[:] = nums[:]
        quit()
    nums_copy = nums.copy()
    for i in range(len(nums)):
        last_idx = len(nums) - 1
        if i + k > last_idx:
            nums[i + k - last_idx - 1] = nums_copy[i]
        else:
            nums[i + k] = nums_copy[i]


print(rotate([1],3))