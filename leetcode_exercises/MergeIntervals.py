# Given an array of intervals where intervals[i] = [starti, endi], merge all overlapping intervals,
# and return an array of the non-overlapping intervals that cover all the intervals in the input.


def merge(intervals: list[list[int]]) -> list[list[int]]:
    intervals.sort()
    ans = []
    counter = 0
    for i in range(len(intervals)-1):
        if intervals[i][-1] >= intervals[i+1][-1]:
            if counter != 0:
                continue
            ans.append([intervals[i]])
            counter +=1
        elif


print(merge([[1, 3], [2, 2], [2, 2], [2, 3], [3, 3], [4, 6], [5, 7]]))