
# A phrase is a palindrome if, after converting all uppercase letters into lowercase letters and removing all non-alphanumeric characters,
# it reads the same forward and backward. Alphanumeric characters include letters and numbers.
#
# Given a string s, return true if it is a palindrome, or false otherwise.

import re


def isPalindrome(s: str) -> bool:
    if "".join(re.findall(r"[A-Za-z0-9]",s)).lower() == "".join(re.findall(r"[A-Za-z0-9]",s)) [::-1].lower():
        return True
    else:
        return False
print(isPalindrome("0P"))

def isPalindrome_2(s: str) -> bool:
    s = s.lower()
    my_s = ''
    for i in s:
        if i.isalpha() or i.isdigit():
            my_s += i
    if my_s == my_s[::-1]:
        return True
    else:
        return False

print(isPalindrome_2("0,ass asf ! afacasc"))