




def singleNumber(nums: list[int]) -> int:
    nums.sort()
    for i in range(1, len(nums) - 1, 3):
        if nums[i] != nums[i - 1] or nums[i] != nums[i + 1]:
            if nums[i] != nums[i-1] and nums[i] == nums[i+1]:
                return nums[i-1]
            else:
                return nums[i+1]
    return nums[-1]


print(singleNumber([2,2,3,2]))


def singleNumber2(nums: list[int]) -> int:
    return (sum(set(nums)) * 3 - sum(nums)) //2

