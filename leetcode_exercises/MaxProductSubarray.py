import math
def maxProduct( nums: list[int]) -> int:
    maxim = 0
    n = len(nums)
    if n <= 2:
        return max(max(nums),maxim,sum(nums))

    subarrays = []

    for i in range(n):
        subarray = []
        for j in range(i, n):
            subarray.append(nums[j])
            subarrays.append(math.prod(subarray.copy()))

    return max(subarrays)


print(maxProduct([2,3,-2,4]))