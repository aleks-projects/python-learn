# Given an integer array nums, return the maximum difference between two successive elements in its sorted form.
# If the array contains less than two elements, return 0.
#
# You must write an algorithm that runs in linear time and uses linear extra space.


def maximumGap(nums: list[int]) -> int:
    if len(nums) < 2:
        return 0
    nums.sort()
    maxim = 0
    for i in range(len(nums)-1):
        if nums[i+1] - nums[i] > maxim:
            maxim = nums[i+1] - nums[i]
    return maxim


print(maximumGap([3,6,9,1]))
