def wordBreak( s: str, wordDict: list[str]) -> bool:
    flag = True
    while flag and s:
        for i in wordDict:
            if s[:len(i)] == i:
                s = s[len(i):]
                flag = True
                break
            flag = False
    if flag:
        return True
    else:
        return False


print(wordBreak("catsandog",["cats","dog","sand","and","cat"]))
print(sorted(["cats","dog","abnd","and","cat"]))