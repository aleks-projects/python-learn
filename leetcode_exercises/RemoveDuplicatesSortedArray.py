#Given an integer array nums sorted in non-decreasing order,
# remove some duplicates in-place such that each unique element appears at most twice.
# The relative order of the elements should be kept the same.

# Since it is impossible to change the length of the array in some languages,
# you must instead have the result be placed in the first part of the array nums.
# More formally, if there are k elements after removing the duplicates, then the first k elements of nums should hold the final result.
# It does not matter what you leave beyond the first k elements.
#
# Return k after placing the final result in the first k slots of nums.


def removeDuplicates(nums: list[int]) -> int:
    my_list = []
    counter = 0
    lenght = 0
    flag = nums[0]
    for i in nums:
        if flag != i:
            flag = i
            counter = 0
        if counter == 2:
            continue
        else:
            my_list.append(i)
            lenght += 1
            counter += 1
    nums[:lenght + 1] = my_list[:]
    return lenght
