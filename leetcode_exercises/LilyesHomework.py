
def lilysHomework(arr):
    arr_1 =sorted(arr)
    arr_2 = sorted(arr,reverse=True)
    arr_copy = arr.copy()
    counter = 0
    swaps_1 = 0
    swaps_2 = 0
    while arr != arr_1 and arr_copy != arr_2:
        min_val = min(arr[counter:])
        max_val = max(arr_copy[counter:])
        if arr[counter] != min_val:
            idx = arr.index(min_val)
            arr[counter], arr[idx] = arr[idx], arr[counter]
            swaps_1 +=1

        if arr_copy[counter] != max_val:
            idx = arr_copy.index(max_val)
            arr_copy[counter], arr_copy[idx] = arr_copy[idx], arr_copy[counter]
            swaps_2 += 1
        counter+=1

    return min(swaps_1,swaps_2)

print(lilysHomework([7, 15, 12, 3]))