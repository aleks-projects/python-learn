# Exercise 1: Reverse the tuple
tuple1 = (10, 20, 30, 40, 50)
print("ex1:", tuple1[::-1])

# Exercise 2: Access value 20 from the tuple
# The given tuple is a nested tuple. write a Python program to print the value 20.

tuple2 = ("Orange", [10, 20, 30], (5, 15, 25))
print("ex2:", tuple2[1][1])

# Exercise 3: Create a tuple with single item 50

tuple3 = ("50",)
print("ex3:", tuple3)

# Exercise 4: Unpack the tuple into 4 variables
# Write a program to unpack the following tuple into four variables and display each variable.

tuple4 = (10, 20, 30, 40)
a, b, c, d = tuple4
print("ex4:")
print(a)
print(b)
print(c)
print(d)

# Exercise 5: Swap two tuples in Python.

tuple5 = (11, 22)
tuple_5 = (99, 88)
tuple5, tuple_5 = tuple_5, tuple5
print("ex5: ")
print(tuple5)
print(tuple_5)

# Exercise 6: Copy specific elements from one tuple to a new tuple
# Write a program to copy elements 44 and 55 from the following tuple into a new tuple.

tuple6 = (11, 22, 33, 44, 55, 66)
my_tuple6 = tuple6[3:5]
print("ex6:", my_tuple6)

# Exercise 7: Modify the tuple
# Given is a nested tuple. Write a program to modify the first item (22) of a list inside a following tuple to 222.

tuple7 = (11, [22, 33], 44, 55)
tuple7[1][0] = 222
print("ex7:", tuple7)

#  Exercise 8: Sort a tuple of tuples by 2nd item.

# tuple8 = (('a', 23), ('b', 37), ('c', 11), ('d', 29))
# print(my_tuple8)


#  Exercise 9: Counts the number of occurrences of item 50 from a tuple.
tuple9 = (50, 10, 60, 70, 50)
print("ex9:", tuple9.count(50))


# Exercise 10: Check if all items in the tuple are the same.

def check_the_same_tuple(my_tuples):
    good_tuple = my_tuples[0]
    for i in range(1, len(my_tuples)):
        if tuple10[i] != good_tuple:
            return False
    return True


tuple10 = ("a", "a", "a", "a")
print(check_the_same_tuple(tuple10))
