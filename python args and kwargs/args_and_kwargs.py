def sum_of_digits(*digits):
    suma = 0
    for i in digits:
        suma += i
    return suma


print(sum_of_digits(1, 5, 10, 2, 5))


def percentage(**kwargs):
    for i in kwargs:
        print(i)
        sub_name = i
        sub_marks = kwargs[i]
        print(sub_name, "=", sub_marks)


percentage(math=56, english=61, science=73)


def my_funktion(**kwargs):
    for i in kwargs:
        print(i, "=", kwargs[i])


my_funktion(a=14, b=48, c=45)


def my_function2(*args, **kwargs):
    lista_modulo_2_args = list()
    lista_modulo_2_kwargs = list()
    for i in args:
        if i % 2 == 0:
            lista_modulo_2_args.append(i)
    for j in kwargs:
        if kwargs[j] % 2 == 0:
            lista_modulo_2_kwargs.append(kwargs[j])
    print(lista_modulo_2_args)
    print(lista_modulo_2_kwargs)


my_function2(1, 3, 5, 8, 10, a=7, b=9, c=10, d=22)